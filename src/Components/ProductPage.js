import React from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { } from 'reactstrap';
import Products from './Items/Details';

const ProductPage = () => (
  <Router>
    <div>
      <div className = "row">
      <div className= "container-fluid" >
        <h1> Luxury Mobile </h1>
        <div className = "col-md-3" >
          <ul className = "Menu-Left">
            <li>
              <Link to="/">Iphone</Link>
            </li>
            <li>
              <Link to="/samsung">SamSung</Link>
            </li>
            <li>
              <Link to="/htc">Htc</Link>
            </li>
          </ul>
        </div>
        <div className = "col-md-9" >
          <div className = "Product-Right">
            <Route exact path="/" render={props => <Iphone {...props} />} />
            <Route path="/samsung" render={props => <Samsung {...props} /> }/>
            <Route path="/htc" render={props => <Htc {...props} />} />
        </div>
        </div>
        </div>
      </div>
    </div>
  </Router>
);

const Iphone = (props) => (
  <div>
    <h2>Iphone List</h2>
    <Products {...props} />
  </div>
);

const Samsung = (props) => (
  <div>
    <h2>SamSung List</h2>
    <Products {...props}/>
  </div>
);

const Htc = (props) => (
  <div>
    <h2>HTC List</h2>
    <Products {...props}/>
  </div>
);

export default ProductPage;