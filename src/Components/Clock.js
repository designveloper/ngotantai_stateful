import React, { Component } from 'react';

class Clock extends Component{
    constructor(props){
        super(props);
        this.state = {seconds : 180};
    }

    decreasementCounter(){
        this.setState(
            (prevState, props) =>({
                seconds: prevState.seconds - 1
            })
        );
    }
    componentDidMount(){
        this.timerID = setInterval(
            () => this.decreasementCounter(), 1000
        );
    }
    componentWillMount(){
        clearInterval(this.timerID);
        
    }
    shouldComponentUpdate(nextProps, nextState){
        if (nextState.seconds === 0){
            clearInterval(this.timerID);
        }
        return true;
    }
    render(){
        console.log(this.state.seconds);
        return(
            <div>
                <h3 style={{textAlign: 'center'}} > Coundown's Clock </h3>
                <p style={{textAlign: 'center'}}> Count: <span style={{color: 'red'}}>{this.state.seconds} s </span> </p>
            </div>
        )
    }
}
export default Clock;